var predmet = new Object();

/* prvi način definiranja svojstava objekta predmet
predmet.naziv = "Web programiranje 1";
predmet.predavac = "Tomislav Adamović";
predmet.semester = 3;
predmet.brojStudenata = 30;
predmet.labosi = true;
predmet.prisutni = 0;*/

//drugi način definiranja svojstava objekta predmet
var predmet = {
   naziv: "Web programiranje 1",
   predavac: "Tomislav Adamović",
   semester: 3,
   brojStudenata: 30,
   labosi: true,
   prisutni:0,
   updatePrisutni: function(){
	   return ++predmet.prisutni;   
   }  
}

//moćemo pristupiti cijelom objektu
console.log(predmet);

//ili preko . operatora  možemo pristupiti svakom svojstvu objekta
console.log(predmet.naziv);

//pregled prisutnih prije poziva metode
console.log('Prisutni prije poziva ' + predmet.prisutni);
//poziv metode za povećanje broja prisutnih studenata
console.log('Poziv updatePrisutni ' + predmet.updatePrisutni());
//pregled prisutnih stidenata nakon poziva metode
console.log('Prisutni nakon poziva ' + predmet.prisutni);