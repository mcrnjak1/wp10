//anonimna funkcija 
//ovdje deklariramo varijablu u kojoj ćemo dodijeliti funkciju 

var veciRazlomak = function(x,y) {
    var result;
    if (x>y){ 
	   result = "Prvi razlomak je veći " + x + " > " + y;
	}else if (x == y){
	   result = "x = y";	
	}else{
	   result ="Drugi razlomak je veći " + y + " > " + x;
	}
	return result;
}
//poziv funkcije
console.log(veciRazlomak(7/9,13/25));
//što ako želimo prikazati vrijednost varijable bez argumenata?
console.log(veciRazlomak);
//dobit ćemo ispis cijele funkcije, ovo će biti bitno kod rada s eventima

//Kako anonimnu funkciju pretvoriti u Immediately invoked funkciju
//najprije cijelu funkciju omeđimo s zagradama () i zatim joj dodamo argumente na kraju

var immediatedVeciRazlomak = (function(x,y) {
    var result;
    if (x>y){ 
	   result = "Prvi razlomak je veći " + x + " > " + y;
	}else if (x == y){
	   result = "x = y";	
	}else{
	   result ="Drugi razlomak je veći " + y + " > " + x;
	}
	return result;
})(3/4,5/6);

//na ovaj način smo dobili funkciju koja će se automatki pokrenuti čim browser dođe do nje
//sljedeća linija nije poziv već ispis vrijednosti varijable

console.log(immediatedVeciRazlomak);

//Ako želimo Immediately invoked funkciji proslijediti varijable tada varijable moraju biti deklarirane prije koda funkcije

var a = 7/8;
var b = 15/16
var immediatedVeciRazlomak = (function(x,y) {
    var result;
    if (x>y){ 
	   result = "Prvi razlomak je veći " + x + " > " + y;
	}else if (x == y){
	   result = "x = y";	
	}else{
	   result ="Drugi razlomak je veći " + y + " > " + x;
	}
	return result;
})(a,b);

console.log(immediatedVeciRazlomak);






