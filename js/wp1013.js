function Predmet(naziv, predavac, semestar, brojStudenata, labosi,prisutni){
   this.naziv = naziv;
   this.predavac = predavac;
   this.semestar = semestar;
   this.brojStudenata = brojStudenata;
   this.labosi = labosi;
   this.prisutni = prisutni;
   this.updatePrisutni = function(){ 
      return ++this.prisutni;
   }
}

//Sada možemo doefinirati objekte, a da ne moramo ponovno pisati nazive svih svojstava i metoda

var predmet01 = new Predmet("Web programiranje 1", "Tomislav Adamović",3,30,true,0);
predmet01.updatePrisutni();
console.log(predmet01);

var predmet02 = new Predmet("Baze podataka", "Tomislav Adamović",3,30,true,0);
predmet02.updatePrisutni();
predmet02.updatePrisutni();
console.log(predmet02);
console.log(predmet02["naziv"]);
console.log(predmet02.naziv);

//možemo definirati i array svih predmate 

var treciSemestar = [
new Predmet("Web programiranje 1", "Tomislav Adamović",3,30,true,0),
new Predmet("Baze podataka", "Tomislav Adamović",3,30,true,0)
]

console.log(treciSemestar);
console.log(treciSemestar[1].naziv);


